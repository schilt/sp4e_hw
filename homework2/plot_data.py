# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 21:53:03 2022

Script to read data (generated in C++) from file and plot it.

"""

import pandas as pd
import matplotlib.pylab as plt

def main():
  
    #filePath = "iterations.csv"
    #filePath = "iterations.psv"
    filePath = "iterations.txt"

    df_data = pd.read_csv(filePath,sep=',') # read data from file and save it as dataframe

    print(df_data) # print data to terminal

    '''Plotting the data:'''

    x = df_data['N']
    y = df_data['Result']

    fig, ax = plt.subplots(1,1,dpi=300)
    ax.plot(x,y)
    ax.set(title = "Convergence to Pi", xlabel = "N", ylabel = "Result" )
    fig.savefig("iterations.png") # save plot as .png file
    plt.show()

if __name__ == "__main__":
    main()



