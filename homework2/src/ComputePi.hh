#ifndef COMPUTEPI
#define COMPUTEPI

#include "Series.hh"

class ComputePi: public Series {
    public:
    int N;
    double result;
    double limit;
    ComputePi(int n){
        N = n;
        // result =compute(N);
    }
    double compute(unsigned int N);
    void getAnalyticPrediction(double l);

};



#endif
