#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "RiemannIntegral.hh"



int main() {

  
    std::cout << "Please enter Arithmetic (A), Pi (P), or RiemannIntegral (R):"<< std::endl;
    Series *ptr;
    char APR_ind; // Select series type
    int maxiter;  
    int freq;
    char PW;      // Select print or write
    std::cin >> APR_ind; // argument to decide which series computation is to be instantiated
    std::cout << "Please enter maxiter:"<< std::endl;
    std::cin >> maxiter;
    std::cout << "Please enter the frequency:"<< std::endl;
    std::cin >> freq;
    std::cout << "Please choose:"<< std::endl;
    std::cout << "[W] Write to file:"<< std::endl;
    std::cout << "[P] Print to terminal:"<< std::endl;
    std::cin >> PW;

    
    // Arithmetic:
    if (APR_ind == 'A'){
	std::cout << "Arithmetic Series" << std::endl;
        ComputeArithmetic ca(maxiter);
	if (PW=='P'){
		PrintSeries PS(maxiter,freq,ca,'A',0.001);
	    	PS.dump();
		}
	else if (PW=='W'){
		WriteSeries WS(maxiter,freq,ca,'A',0.001,',');
		WS.dump(std::cout);
		}
    }
    // Pi:
    else if (APR_ind == 'P'){
	std::cout << "Pi Series" << std::endl;
        ComputePi cp(maxiter);
	cp.getAnalyticPrediction(M_PI);
	PrintSeries PS(maxiter,freq,cp,'P',0.1);
	if (PW=='P'){
		PrintSeries PS(maxiter,freq,cp,'P',0.001);
		PS.setPrecision(4);
	    	PS.dump();
		}
	else if (PW=='W'){
		WriteSeries WS(maxiter,freq,cp,'P',0.001,',');
		WS.dump(std::cout);
		}
    }
    // Riemann Integral:
    else if (APR_ind == 'R'){
	std::cout << "Riemann Integral" << std::endl;
	int f;
	double a;
	double b;

	std::cout << "Please select the type of function to be integrated by selecting the corresponding interger number:"<< std::endl;
	std::cout << "1: f=x^3"<< std::endl;
	std::cout << "2: f=cos(x)"<< std::endl;
	std::cout << "3: f=sin(x)"<< std::endl;
	std::cin >> f; // take user input
	std::cout << "Please enter the lower integral boundary a:"<< std::endl;
	std::cin >> a; // take user input
	std::cout << "Please enter the upper integral boundary b:"<< std::endl;
	std::cin >> b; // take user input

    	RiemannIntegral Class_RI(f,a,b,10000); // create an instance of RiemannIntegral and pass user input to this instance
    	std::cout << "Riemann sum: " << Class_RI.result << std::endl;

    }
    // Incorrect user input:
    else {
	std::cout << "incorrect user input" << std::endl;

    }


    //EXERCISE 6, Tasks 3&4:
    std::cout << std::endl << "EXERCISE 6, Tasks 3&4:" << std::endl;

    // Analytic predictions to intervals:
    double integral_x3_0_1 = 0.25;
    double integral_cosx_0_pi = 0.00;
    double integral_sinx_0_05pi = 1.00;

    //******************************************************************************************************************
    // Calculate the number of steps (N) required for the integral of x^3 from 0 to 1:
    double diff = 1.0; // variable where the difference between calculated value and analytic prediction will be stored
    int tmp_count = 1; // temporary counter for while loop
    double tmp_result = 0; // variable to temporarily store the result

    while (diff>=0.01){
    	RiemannIntegral objRI(1,0,1,tmp_count);
       	tmp_result = objRI.result;
	diff = std::abs(tmp_result-integral_x3_0_1); // calculate the difference
	tmp_count +=1; // increase counter by 1
    }

    std::cout << std::endl << "Riemann sum for integral(x^3,0,1): " << tmp_result << std::endl;
    std::cout << "Analytical prediction for integral(x^3,0,1): " << integral_x3_0_1 << std::endl;
    std::cout << "Difference: " << diff << std::endl;
    std::cout << "Number of steps N: " << tmp_count-1 << std::endl;

    //******************************************************************************************************************
    // Calculate the number of steps (N) required for the integral of cos(x) from 0 to pi:

    // reset variables
    diff = 1.0;
    tmp_count = 1;
    tmp_result = 0;

    while (diff>=0.01){
    	RiemannIntegral objRI(2,0,M_PI,tmp_count);
       	tmp_result = objRI.result;
	diff = std::abs(tmp_result-integral_cosx_0_pi); // calculate the difference
	tmp_count +=1; // increase counter by 1
    }

    std::cout << std::endl << "Riemann sum for integral(cos(x),0,pi): " << tmp_result << std::endl;
    std::cout << "Analytical prediction for integral(cos(x),0,pi): " << integral_cosx_0_pi << std::endl;
    std::cout << "Difference: " << diff << std::endl;
    std::cout << "Number of steps N: " << tmp_count-1 << std::endl;

    //******************************************************************************************************************
    // Calculate the number of steps (N) required for the integral of sin(x) from 0 to pi/2:

    // reset variables
    diff = 1.0;
    tmp_count = 1;
    tmp_result = 0;

    while (diff>=0.01){
    	RiemannIntegral objRI(3,0,M_PI_2,tmp_count);
       	tmp_result = objRI.result;
	diff = std::abs(tmp_result-integral_sinx_0_05pi); // calculate the difference
	tmp_count +=1; // increase counter by 1
    }

    std::cout << std::endl << "Riemann sum for integral(sin(x),0,0.5pi): " << tmp_result << std::endl;
    std::cout << "Analytical prediction for integral(sin(x),0,0.5pi): " << integral_sinx_0_05pi << std::endl;
    std::cout << "Difference: " << diff << std::endl;
    std::cout << "Number of steps N: " << tmp_count-1 << std::endl;


    return 0;
}
