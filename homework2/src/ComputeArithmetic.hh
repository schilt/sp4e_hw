#ifndef COMPUTEARITHMETIC
#define COMPUTEARITHMETIC

#include "Series.hh"

class ComputeArithmetic : public Series {
    public: 
    int N;
    //int result;
    double limit;
    ComputeArithmetic(int n){
        N = n;
        current_value = compute(N);
    }// Constructor
    double compute(unsigned int N);
    void getAnalyticPrediction(double l);
};



#endif
