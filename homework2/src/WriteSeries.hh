#ifndef WRITESERIES
#define WRITESERIES

#include <iostream>
#include <fstream>
#include "DumperSeries.hh"
#include "Series.hh"

class WriteSeries: public DumperSeries {
    public:
        int maxiter;
        int frequency;
        double accuracy;
        char AP;
        char sw_file;
        WriteSeries(int n, int f, Series &s, char ap, double acc, char sw=' '):DumperSeries(s) {
            maxiter = n;
            frequency  = f;
            AP = ap;
            accuracy = acc;
            sw_file = sw;
        }
        void dump(std::ostream & os);
};


#endif
