#ifndef SERIES
#define SERIES

#include <iostream>
#include <cmath>
#include <math.h>

class Series{
    public:
    unsigned int current_index = 0;
    double current_value = 0;
    double limit;
    virtual double compute(unsigned int N) = 0;
    virtual void getAnalyticPrediction(double l){ 
        limit= std::nan("1");
    } 
};

#endif
