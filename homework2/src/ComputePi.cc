#include <iostream>
#include <cmath>
#include "ComputePi.hh"
#include "Series.hh"

double ComputePi::compute(unsigned int N){

	double sum = 0;
        for (int i = current_index+1; i < N+1; i++) {
            sum +=  1.0/pow(i,2);
	    current_index = current_index+1;       // Record the indices that have already be calculated
        }
        current_value = (current_value+(6.*sum));  // Record current sum
        return sqrt(current_value);

}

void ComputePi::getAnalyticPrediction(double l){ 
        limit = l;
}
