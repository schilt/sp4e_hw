#include <iostream>
#include <cmath>
#include "RiemannIntegral.hh"
#include "Series.hh"

// method to calculate the function f; function is selected in the main()
double RiemannIntegral::f_xi(int f, double xi){

	if (f==1){
		return pow(xi,3);	
	}
	else if (f==2){
		return cos(xi);
	}
	else if (f==3){
		return sin(xi);
	}

}


// method to compute the riemann sum
double RiemannIntegral::compute(unsigned int N){
	
	double dx = (b-a)/N;
	double x = 0;
	double sum = 0;

        for (int i = 1; i < N+1; i++) {
	    x = a+i*dx;
            sum +=  f_xi(f,x)*dx;
        }
        return sum;

	
}
