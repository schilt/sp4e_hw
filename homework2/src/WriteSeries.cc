#include <iostream>
#include <fstream>
#include <cmath>
#include "Series.hh"
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "DumperSeries.hh"
#include "WriteSeries.hh"


void WriteSeries::dump(std::ostream & os){
            std::ofstream myfile;
            switch(sw_file) {
                case ',':
                    myfile.open ("iterations.csv");
                    break;
                case '|':
                    myfile.open ("iterations.psv");
                    break;
                default:
                    myfile.open ("iterations.txt");
            }
            myfile << "Step,N,Result\n";
            if (AP == 'A'){
                ComputeArithmetic &ptr=dynamic_cast<ComputeArithmetic &>(series);
                double res = 0;
                for (int i = 1; i < int(double(maxiter)/double(frequency))+1; i++) {
                    res = ptr.compute(i*frequency); 
                    myfile << i << "," << i*frequency << ","<< res << "\n";
                }
            } else {
                ComputePi &ptr=dynamic_cast<ComputePi &>(series);
                double res = 0;
                for (int i = 1; i < int(double(maxiter)/double(frequency))+1; i++) {
                    res = ptr.compute(i*frequency);
                    myfile << i << "," << i*frequency << ","<< res << "\n";
                    if (std::isnan(ptr.limit) == false){
                        if (std::abs(res-ptr.limit)<accuracy){
                            std::cout<<"Convergence"<<std::endl;
                            break;
                        }
                    }
                }
            }
            myfile.close();
        }



