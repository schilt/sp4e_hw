#include <iostream>
#include "ComputeArithmetic.hh"
#include "Series.hh"

double ComputeArithmetic::compute(unsigned int N) {
    return N*(N+1)/2;
    }

void ComputeArithmetic::getAnalyticPrediction(double l){ 
        limit= std::nan("1");
    }// For this method the sum does not converge, this function returns NAN
