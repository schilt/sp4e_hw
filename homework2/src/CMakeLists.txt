
cmake_minimum_required(VERSION 3.0)
project(Homework2)

add_executable(SeriesExe main.cc ComputeArithmetic.cc ComputePi.cc RiemannIntegral.cc PrintSeries.cc WriteSeries.cc)
#add_executable(SeriesExe main.cc ComputeArithmetic.cc ComputePi.cc PrintSeries.cc)
#add_executable(SeriesExeTest Hw2_Ex3.cpp) #for testing only
