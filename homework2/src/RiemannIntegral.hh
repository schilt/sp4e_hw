#ifndef RIEMANNINTEGRAL
#define RIEMANNINTEGRAL

#include "Series.hh"

class RiemannIntegral : public Series {
    
    // members:
    public: 
    int N;
    double result;
    double a;
    double b;
    int f; // define which function should be used: 1=x^3, 2=cos(x), 3=sin(x)

    // constructor:
    RiemannIntegral(int arg_f, double arg_a, double arg_b, int n){
        N = n;
        a = arg_a;
	b = arg_b;
	f = arg_f;
	result = compute(N);
    }

    // methods:
    double compute(unsigned int N);

    double f_xi(int f, double xi);


};



#endif
