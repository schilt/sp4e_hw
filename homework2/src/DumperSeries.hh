#ifndef DUMPERSERIES
#define DUMPERSERIES
#include <iostream>
#include "Series.hh"

class DumperSeries{
    public:
        virtual void dump(std::ostream & os) = 0;
	virtual void setPrecision(unsigned int precision){};

    /*	
    inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
    }
    */
    

    protected:
        Series & series;
        DumperSeries(Series &s) :series(s){ }
	

};

#endif
