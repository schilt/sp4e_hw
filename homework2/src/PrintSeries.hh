#ifndef PRINTSERIES
#define PRINTSERIES

#include <iostream>
#include "DumperSeries.hh"
#include "Series.hh"

class PrintSeries: public DumperSeries {
    public:
        int maxiter;
        int frequency;
        double accuracy;
        char AP;

        PrintSeries(int n, int f, Series &s, char ap, double acc):DumperSeries(s) {
            maxiter = n;
            frequency  = f;
            AP = ap;
            accuracy = acc;
        }

        void dump(std::ostream & os = std::cout);

	void setPrecision(unsigned int precision);
   
};


#endif
