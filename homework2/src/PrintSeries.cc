#include <iostream>
#include <cmath>
#include "Series.hh"
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "DumperSeries.hh"
#include "PrintSeries.hh"

void PrintSeries::setPrecision(unsigned int precision){

	accuracy = 1.0/pow(10.0,double(precision));
	//std::cout << "ACCURACY: " << accuracy << std::endl;

}

// define the method dump():
void PrintSeries::dump(std::ostream & os /*= std::cout*/){

            if (AP == 'A'){
                ComputeArithmetic &ptr=dynamic_cast<ComputeArithmetic &>(series);               // transform series back to the daughterclass
                double res = 0;
                for (int i = 1; i < int(double(maxiter)/double(frequency))+1; i++) {
                    res = ptr.compute(i*frequency); 
                    os << "Step: " << i << "; N: " << i*frequency << "; Result: "<< res << std::endl;
                }
            } else {
                ComputePi &ptr=dynamic_cast<ComputePi &>(series);				// transform series back to the daughterclass
                double res = 0;
                for (int i = 1; i < int(double(maxiter)/double(frequency))+1; i++) {
                    res = ptr.compute(i*frequency);
                    os << "Step: " << i << "; N: " << i*frequency << "; Result: "<< res << std::endl;
                    if (std::isnan(ptr.limit) == false){
                        if (std::abs(res-ptr.limit)<accuracy){
                            os<<"Convergence"<<std::endl;
                            break;
                        }
                    }
                }
            }
            
        }



