Student names:
Baiwei Guo
Ueli Schilt

Exercise 2, Task 1. What is the best way/strategy to divide the work among yourselves? Please answer this question in
the README file.

Answer:
In a first round we discuss each task together and possible approaches. Then we divide the work as follows:
- One person implements the methods and tests if they are working as intended
- The other person organises the methods and classes in the correct order

INSTRUCTIONS to launch the code:

Folder structure:

homework2
-- src
   -- CMakeLists.txt
   -- main.cc
   -- Series.hh
   -- ComputeArithmetic.hh
   -- ComputeArithmetic.cc
   -- ComputePi.hh
   -- ComputePi.cc
   -- DumperSeries.hh
   -- PrintSeries.hh
   -- PrintSeries.cc
   -- WriteSeries.hh
   -- WriteSeries.cc
   -- RiemannIntegral.hh
   -- RiemannIntegral.cc
-- CMakeLists.txt
-- plot_data.py
-- README.md

How to launch the C++ code:

1. Launch terminal in 'homework2' folder
2. Carry out build with cmake:
   a. $ mkdir build
   b. $ cd build
   c. $ ccmake ..
      - configure
      - generate
   d. $ make
3. In terminal, navigate to folder containing executable 'SeriesExe': $ ./homework2/build/src
4. In terminal, open executable: $ ./SeriesExe
5. You are asked to enter the type of series computation: 'A' for Arithmetic, 'P' for Pi, 'R' for Riemann Integral
6. Code will return the calculated value; calculations for specific integrals of Exercise 6 are calculated automatically
7. The iteration.csv/psv/txt file is printed at the same directory as the executable.

How to launch Python code:
1. The pyhton script is located in the 'homework2' folder
2. In the script, adjust the parameter 'filePath' to the location of the results file (e.g. filePath = "iterations.csv")
3. Launch the python script from the terminal ($ python plot_data.py)
4. The plot will be saved as a .png file in the same folder where the python script is stored

Answer to Exercise 5, 
Task 1: We discuss the complexity involved in the calculation of the sum of the series when PrintSeries/WriteSeries is instanciated.
For ComputeArithmegtic, the complexity of printing n steps is O(n).
For ComputePi, the complexity of printing n steps is O(n^2).
Task 4: 
For ComputeArithmegtic, the complexity of printing n steps is O(n).
For ComputePi, the complexity of printing n steps is O(n).
Task 5: 
The best is still O(n).


