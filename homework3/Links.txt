Links:

2d fftw:

https://github.com/jonathanschilling/fftw_tutorial#2d-complex-to-complex

https://cse.usf.edu/~goldgof/FFT/fftw_3.html

https://gibbs.ccny.cuny.edu/technical/Notes/FFTW/fftw3.pdf  (GOOD)

Frequencies:

https://docs.scipy.org/doc/scipy/reference/generated/scipy.fftpack.fftfreq.html

https://stackoverflow.com/questions/7161417/how-to-calculate-wavenumber-domain-coordinates-from-a-2d-fft
