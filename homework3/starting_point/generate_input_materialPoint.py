import numpy as np
import math
import sys
import argparse

# toggle to create input for respective exercise; 0 = no specific exercise; 1 = exercise 4.5
toggleExercise = 1

parser = argparse.ArgumentParser()
parser.add_argument("number", help="number of particles", type=int)
parser.add_argument("filename", help="name of generated input file")

args = parser.parse_args()

# initialize arrays:
positions = np.zeros((args.number,3)) 
velocity = np.zeros((args.number, 3))
force = velocity.copy()
masses = 1e9*(np.random.random((args.number, 1)) + 1)
temperature = np.zeros((args.number, 1))
heat_rate = np.zeros((args.number, 1))
temp_bc_marker = np.zeros((args.number, 1)) # marker for temperature boundary conditions (bc) that can be passed on to computation; if=0: no bc



if toggleExercise == 0: # no specific exercise

	#----------------------------------------------------------------------------#
	# allocation of constant temperature and heat rate
	# allocation of material point positions for x [-1,1] and y [-1,1]:

	x0 = -1
	xN = 1
	y0 = -1
	yN = 1

	Lx = xN-x0
	Ly = yN-y0
	
	Tc = 10 # constant temperature
	hrc = 1 # constant heat rate

	
	x = 0
	y = 0
	z = 0
	N = int(math.sqrt(args.number)) # side of matrix

	i=0
	j=0

	while i<N:
		j=0
	    
		while j<N:
			x = x0 + Lx/(N)*i
			y = y0 + Ly/(N)*j

			positions[j+N*i][0] = x
			positions[j+N*i][1] = y
			positions[j+N*i][2] = z
			
			temperature[j+N*i] = Tc
			heat_rate[j+N*i] = hrc

			j+=1
	     
		i+=1
	#----------------------------------------------------------------------------#  

elif toggleExercise == 1: # Input according Exercise 4.5
	#----------------------------------------------------------------------------#
	# Parameters:

	"""Parameters"""
	N = int(math.sqrt(args.number)) # side of matrix
	R = 1 # radius
	L = 4*R # side of considered space (on purpose larger than 2R, to include points outside of R)
	
	#----------------------------------------------------------------------------#
	# Heat and temperature distribution and positions of Exercise 4.5:
	
	T0 = 1 # initial temperature

	i = 0
	j = 0

	x = 0
	y = 0
	z = 0

	"""Heat distribution"""

	while i<N:
	    
		j = 0
		
		#print("Row {}\n".format(i))

		while j<N:
			# coordinates
			x = -L/2 + i/(N)*L
			y = -L/2 + j/(N)*L
			
			positions[j+N*i][0] = x
			positions[j+N*i][1] = y
			positions[j+N*i][2] = z

			if (np.square(x)+np.square(y))<R:
			    
			    heat_rate[j+i*N] = 1
			    temperature[j+i*N] = T0
			    temp_bc_marker[j+N*i] = 0 # here no boundary condition applies
			    
			else:
			    
			    heat_rate[j+i*N] = 0
			    temperature[j+i*N] = 0
			    temp_bc_marker[j+N*i] = 1 # here boundary condition applies (T_ij=0)

			#print(temp_bc_marker[j+N*i],"	")

			j+=1
			
			

		i+=1

	#----------------------------------------------------------------------------#

else:
	print("Incorrect value chosen for toggleExercise. Must be 0 or 1.")
	sys.exit();

     

file_data = np.hstack((positions, velocity, force, masses,temperature,heat_rate, temp_bc_marker))
np.savetxt(args.filename, file_data, delimiter=" ")
