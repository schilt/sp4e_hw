#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "matrix.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Constructor
public:
  ComputeTemperature(Real timestep, Real arg_L=2, Real arg_C=1, Real arg_rho=1, Real arg_kappa=-1);

  // Methods
public:

  //! Set L
  void setL(Real L);
  
  //! Set C
  void setC(Real C);
  
  //! Set rho
  void setRho(Real rho);
  
  //! Set kappa
  void setKappa(Real kappa);

  //! Compute temperature implementation
  void compute(System& system) override;
  

  
  /*
  void ftw_to_matrix(double (*F_obj)[N][2], Matrix<complex> &M_obj);
  */
  
  void compute_kappa(Matrix<std::complex<double>> &fft_coord, Matrix<complex> &To_fft, Matrix<complex> &hv_fft, UInt N);
  
  double pwc_linear_To(double y);
  
  double point_heat(double y);
  

  void load_data(int k, Matrix<complex> &hv, Matrix<complex> &To, UInt N);
  
  

private:

 // parameters:
 Real L;
 Real C;
 Real rho;
 Real kappa = -1; // initialise with -1 to see if the user makes a change

 Real dt;
 
 

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
