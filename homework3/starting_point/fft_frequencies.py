import numpy as np
#from numpy.fft import fftfreqs


print("1D signal even n=8: \n")

signal = np.array([1, 1, 1, 1, 1, 1, 1, 1], dtype=float)

fourier = np.fft.fft(signal)

n = signal.size

timestep = 1.0

freq = np.fft.fftfreq(n, d=timestep)

print(freq)


print("1D signal odd n=9: \n")

signal = np.array([1, 1, 1, 1, 1,1,1,1,1], dtype=float)

fourier = np.fft.fft(signal)

n = signal.size

timestep = 1.0

freq = np.fft.fftfreq(n, d=timestep)

print(freq)

print("\n 2D signal:")

signal = np.array([[-2, 8, 6, 4, 1, 0, 3, 5],[-2, 8, 6, 4, 1, 0, 3, 5]], dtype=float)

fourier = np.fft.fft2(signal)

#n = signal.size

timestep = 1.0

#freq = np.fft.fftfreq(n, d=timestep)

FreqCompRows = np.fft.fftfreq(signal.shape[0],d=1)
FreqCompCols = np.fft.fftfreq(signal.shape[1],d=1)


print(FreqCompRows)
print(FreqCompCols)

print("\n 2D signal again:")

signal = np.array([[3, 3, 3],[ 3, 3, 3],[3, 3, 3]], dtype=float)

fourier = np.fft.fft2(signal)

#n = signal.size

timestep = 1.0

#freq = np.fft.fftfreq(n, d=timestep)

FreqCompRows = np.fft.fftfreq(signal.shape[0],d=1)
FreqCompCols = np.fft.fftfreq(signal.shape[1],d=1)


print(FreqCompRows)
print(FreqCompCols)

