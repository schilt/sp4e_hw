#ifndef __SYSTEM_EVOLUTION__HH__
#define __SYSTEM_EVOLUTION__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "system.hh"
/* -------------------------------------------------------------------------- */

//! Manager for system evolution
class SystemEvolution {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  //! Construct using existing system (takes ownership)
  SystemEvolution(std::unique_ptr<System> system);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  //! Evolve all time steps
  void evolve();
  //! Add compute to list of computes
  void addCompute(const std::shared_ptr<Compute>& compute);
  //! Get the system object
  System& getSystem();

  void setNSteps(UInt nsteps);
  void setDumpFreq(UInt freq);
  
  // parameters of heat equation:
  void setL(Real L) { this->L = L; }
  Real getL() { return L; }
  
  void setRho(Real rho) { this->rho = rho; }
  Real getRho() { return rho; }
  
  void setC(Real C) { this->C = C; }
  Real getC() { return C; }
  
  void setKappa(Real kappa) { this->kappa = kappa; }
  Real getKappa() { return kappa; }
  
  // toggle to activate boundary condition of exercise 4.5
  void setToggle(UInt num) { this->toggle_exercise = num; }
  UInt getToggle() { return toggle_exercise; }

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
protected:
  std::vector<std::shared_ptr<Compute>> computes;
  std::unique_ptr<System> system;
  UInt nsteps, freq;
  
  // parameters of heat equation:
  Real L;
  Real rho;
  Real C;
  Real kappa;
  
  // toggle to activate boundary condition of exercise 4.5:
  UInt toggle_exercise = 0;

};

/* -------------------------------------------------------------------------- */
#endif  //__SYSTEM_EVOLUTION__HH__
