#ifndef __SYSTEM__HH__
#define __SYSTEM__HH__

/* -------------------------------------------------------------------------- */
#include "my_types.hh"
#include "particle.hh"
#include <memory>
#include <vector>
/* -------------------------------------------------------------------------- */

//! Container for particles
class System {

public:
  virtual ~System() = default;

  // List of particle pointers
  using ParticleList = std::vector<std::shared_ptr<Particle>>;

  //! Remove particle from vector
  void removeParticle(UInt particle){};
  //! Get particle for specific id
  Particle& getParticle(UInt i);
  //! Add a particle to the system
  void addParticle(const std::shared_ptr<Particle>& new_particle);
  //! Get number of particles
  UInt getNbParticles();

  //! Iterator class to erase the unique pointer on Particle
  struct iterator : ParticleList::iterator {
    iterator(const ParticleList::iterator& it) : ParticleList::iterator(it) {}

    //! Access the underlying particle
    Particle& operator*() { return *ParticleList::iterator::operator*(); }
  };

  // Iterators
public:
  auto begin() { return iterator(list_particles.begin()); }
  auto end() { return iterator(list_particles.end()); }
  
  // parameters of heat equation:
  void setL(Real L) { this->L = L; }
  Real getL() { return L; }
  
  void setRho(Real rho) { this->rho = rho; }
  Real getRho() { return rho; }
  
  void setC(Real C) { this->C = C; }
  Real getC() { return C; }
  
  void setKappa(Real kappa) { this->kappa = kappa; }
  Real getKappa() { return kappa; }
  
  // toggle to activate boundary condition of exercise 4.5:
  void setToggle(UInt num) { this->toggle_exercise = num; }
  UInt getToggle() { return toggle_exercise; }

protected:
  ParticleList list_particles;
  
  // parameters of heat equation (with default values):
  Real L = 2;
  Real rho = 1;
  Real C = 1;
  Real kappa = -1;
  
  // toggle to activate boundary condition of exercise 4.5:
  UInt toggle_exercise = 0; // 0=no exercise; 1 = Exercise 4.5 (apply boundary condition)

};

/* -------------------------------------------------------------------------- */

#endif  //__SYSTEM__HH__
