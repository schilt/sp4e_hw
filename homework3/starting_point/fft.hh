#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<double>> computeFrequencies(int size, double L);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) { 

  UInt N = m_in.rows(); // get the size of input matrix (assumed to be square)
  Matrix<complex> m_out(N);

  auto pointer_in = (fftw_complex*)m_in.data();
  auto pointer_out = (fftw_complex*)m_out.data();
  
  auto plan = fftw_plan_dft_2d(N, N, pointer_in, pointer_out, FFTW_FORWARD, FFTW_ESTIMATE);
  
  // carry out transformation:
  fftw_execute(plan);
    
  fftw_destroy_plan(plan);

  fftw_cleanup();
  
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  
  /*--------------------------------------------------------------------------------------------------*/
  //NEW
  
 
  UInt N = m_in.rows(); // get the size of input matrix (assumed to be square)
  Matrix<complex> m_out(N);

  auto pointer_in = (fftw_complex*)m_in.data();
  auto pointer_out = (fftw_complex*)m_out.data();
  
  auto plan = fftw_plan_dft_2d(N, N, pointer_in, pointer_out, FFTW_BACKWARD, FFTW_ESTIMATE);
  
  // carry out transformation:
  fftw_execute(plan);
  
  // normalize
  m_out/=((N*N));
   
  fftw_destroy_plan(plan);

  fftw_cleanup();
  
  return m_out;

}

/* ------------------------------------------------------ */

inline Matrix<std::complex<double>> FFT::computeFrequencies(int size, double L = 1) {

	UInt N = size;

	bool toggle_even;

	int frequency[N]; // create array of size N

	// check if N is even or odd:
	if (N % 2 == 0) // even
	    toggle_even = true;
	else // odd
	    toggle_even = false;
	    
	int tmp_counter = 0; // used for the array index
	    
	if (toggle_even == true) { // for even size

	    for (int i=0;i<(N/2);i++) {
	    
		frequency[tmp_counter] = i;
		tmp_counter++;
	    
	    }
	    
	    for (int i=-(N/2);i<=-1;i++) {
	    
		frequency[tmp_counter] = i;
		tmp_counter++;
	    }

	}

	else if (toggle_even == false) { // for odd size


	    for (int i=0;i<=((N-1)/2);i++) {
	    
		frequency[tmp_counter] = i;
		tmp_counter++;
	    }
	    
	    for (int i=-((N-1)/2);i<=-1;i++) {
	    
		frequency[tmp_counter] = i;
		tmp_counter++;
	    }

	}


	Matrix<std::complex<double>> M_obj(N); // create return matrix


	int idx_r; // row index
	int idx_c; // column index

	// popultate the return matrix with wavenumber values:
	for (idx_r = 0;idx_r<N;idx_r++) {

		for (idx_c = 0;idx_c<N;idx_c++) { 
		    
			M_obj(idx_r,idx_c)= double(pow(frequency[idx_r],2) + pow(frequency[idx_c],2))/(pow(L,2));
		
		}
	}

	return M_obj;

}



#endif  // FFT_HH
