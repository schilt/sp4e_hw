#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);
  
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
    //val = sin(k * i);
  }
  
  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {

  UInt N = 512;
  Matrix<complex> a(N);
  
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(a)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i == 1 && j == 0) {
      val = N * N / 2;}
    else if (i == N - 1 && j == 0) {
      val = N * N / 2;}
    else {
      
    val = 0;
    }
  }
  

  Matrix<complex> res = FFT::itransform(a);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    ASSERT_NEAR(val.real(), cos(k * i), 1e-10);

  }


}
/*****************************************************************/

TEST(FFTFREQ, compute_wavenumbers) {

  UInt N = 512;  
  Matrix<std::complex<double>> res = FFT::computeFrequencies(N,1);
  
  int i;
  int j;
  
  
  for (i=0;i<N;i++) {
  
  	for (j=0;j<N;j++) {
  	    
  	    // test for symmetry:
  	    ASSERT_NEAR(res(i,j).real(), res(j,i).real(), 1e-10);
  	    
  	    // test first and last entry:
  	    if (i==0 && j==0) {
  	    
  	        ASSERT_NEAR(res(i,j).real(), 0, 1e-10);
  	    
  	    }
  	    
  	    else if (i==(N-1) && j==(N-1)) {
  	    
  	        ASSERT_NEAR(res(i,j).real(), 2, 1e-10);
  	    
  	    }
  	
  	}
  
  }
	// TEST TO BE IMPLEMENTED! -> test for symmetry

}
/*****************************************************************/
