#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include "fft.hh"
#include "my_types.hh"
#include "matrix.hh"
#include <gtest/gtest.h>
#include <cmath>

/*****************************************************************/
// Fixture class
class MaterialPointsTemperature : public ::testing::Test {
protected:
  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> material_points;
    n_points = 256;
    N = sqrt(n_points);
    dt = 0.1;
    
    temperature = std::make_shared<ComputeTemperature>(dt);
    
    for (UInt i = 0; i < n_points; ++i) {
      MaterialPoint p;
      material_points.push_back(p);
    }

    for (auto& p : material_points) {
      // std::cout << p << std::endl;
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }
    
  }
  
  // helper function for equilibrium temperature in Exercise 4.4:
  double pwc_linear_To(double y){
	double x;
	if (y <=double(-1)/2){
		x = -1 -y;
	}
	if (y <= double(1)/2 && y > double(-1)/2){
		x = y;
	}
	if (y > double(1)/2){
		x = 1 - y;
	}
	return x;
  }
  
  // helper function for heat source in Exercise 4.4:
  double point_heat(double y){
	double x;
	if (y ==double(-1)/2) { 
		x = -1;
	}
	else if (y ==double(1)/2) {
		x = 1;
	}
	else {
		x = 0;
	}

	return x;
  }

  System system;
  Real dt; // timestep
  UInt n_points; // number of points
  UInt N; // matrix side
  
  std::shared_ptr<ComputeTemperature> temperature;
};

/*****************************************************************/

TEST_F(MaterialPointsTemperature, no_heat_flux_Ex4_2) { // Exercise 4.2

UInt r,i;
double x;
Matrix<complex> hv(N); // volumetric heat source
Matrix<complex> To(N); // temperature field

Real Th = 1; // homogeneous temperature

Real Tn; // computed temperature after one timestep

// load data for volumeetric heat source and initial temperature field at equilibrium:
for (r = 0; r < N; r++) {
    for (i = 0; i < N; i++) {
	    	x = -1 + 2 * double(r) / N;
	    	hv(r,i) = 0.0; // no heat flux
	    	To(r,i) = Th;
	    }
    }
   
// get temperature and heat rates and write it to system:	
for (r=0;r<N;r++) {
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		mpp.getTemperature() = To(r,i).real();
		mpp.getHeatRate() = hv(r,i).real();		
		
	}
}

// set parameters:
system.setRho(1);
system.setC(1);
system.setKappa(1);

// compute temperature propagation:
temperature->compute(system);

// carry out test:
for (r=0;r<N;r++) {

	//printf("\n Row %d\nTn	vs.	To\n",r);
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		Tn = mpp.getTemperature();
		
		//printf("%g	vs.	%g\n",Tn,To(r,i).real());
		
		ASSERT_NEAR(Tn, To(r,i).real(), 1e-10);	
		
	}
}

}

/*****************************************************************/
TEST_F(MaterialPointsTemperature, volumetric_heat_source_Ex4_3) { // Exercise 4.3

UInt r,i;
double x;
Matrix<complex> hv(N); // volumetric heat source
Matrix<complex> To(N); // temperature field
Real L = 2;

Real Tn; // computed temperature after one timestep

// load data for volumeetric heat source and initial temperature field at equilibrium:
for (r = 0; r < N; r++) {
    for (i = 0; i < N; i++) {
	    	x = -1 + 2 * double(r) / (N);
	    	hv(r,i) = pow(2 * M_PI/L,2) * sin(2 * M_PI * x / L);  
	    	To(r,i) = sin(2 * M_PI * x / L);
	    }
    }
   
// get temperature and heat rates and write it to system:	
for (r=0;r<N;r++) {
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		mpp.getTemperature() = To(r,i).real();
		mpp.getHeatRate() = hv(r,i).real();		
		
	}
}

temperature->setKappa(-1); // if kappa<0, it will be computed in compute

// compute temperature propagation:
temperature->compute(system);

//carry out test for each point:
for (r=0;r<N;r++) {

	//printf("\n Row %d\nTn	vs.	To\n",r);
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		Tn = mpp.getTemperature();
		
		//printf("%g	vs.	%g\n",Tn,To(r,i).real());
		
		ASSERT_NEAR(Tn, To(r,i).real(), 1e-10);	
		
	}
}


}
/**************************************************************************/

TEST_F(MaterialPointsTemperature, volumetric_heat_source_Ex4_4) { // Exercise 4.4

// UNDER CONSTRUCTION

UInt r,i;
double x;
Matrix<complex> hv(N); // volumetric heat source
Matrix<complex> To(N); // temperature field

Real Tn; // computed temperature after one timestep

// set parameters:
system.setRho(1);
system.setC(1);
system.setKappa(1);
system.setL(2);

// load data for volumeetric heat source and initial temperature field at equilibrium:
for (r = 0; r < N; r++) {
    for (i = 0; i < N; i++) {
	    	x = -1 + 2 * double(r) / (N);
	    	hv(r,i) = point_heat(x);  
	    	To(r,i) = pwc_linear_To(x);
	    	
	    	/*
	    	if (i == 0){
	    		printf("x: %f, hv: %f, To: %f\n", x, hv(r,i).real(), To(r,i).real());
	    	}
	    	*/
	    }
    }

   
// get temperature and heat rates and write it to system:	
for (r=0;r<N;r++) {
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		mpp.getTemperature() = To(r,i).real();
		mpp.getHeatRate() = hv(r,i).real();		
		
	}
}

// compute temperature propagation:
temperature->compute(system);

//carry out test:
for (r=0;r<N;r++) {

	//printf("\n Row %d\nTn	vs.	To\n",r);
	for (i=0;i<N;i++) {
	
		Particle& par = system.getParticle(i + r*N);
		  
		auto& mpp = static_cast<MaterialPoint&>(par);

		Tn = mpp.getTemperature();
		
		//printf("%g	vs.	%g\n",Tn,To(r,i).real());
		
		ASSERT_NEAR(Tn, To(r,i).real(), 1e-10);	
		
	}
}

Particle& par2 = system.getParticle(12 + 13*16);

auto& mpp2 = static_cast<MaterialPoint&>(par2);

Real T2 = mpp2.getTemperature();

//printf("Temperature: %g\n",T2);

}

