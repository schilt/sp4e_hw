#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include "matrix.hh"
#include <cmath>


/* -------------------------------------------------------------------------- */

// constructor passes timestep to dt member:
ComputeTemperature::ComputeTemperature(Real timestep, Real arg_L, Real arg_C, Real arg_rho, Real arg_kappa) {

  this->dt = timestep;
  this->L = arg_L;
  this->C = arg_C;
  this->rho = arg_rho;
  this->kappa = arg_kappa;
  
  }

/* -------------------------------------------------------------------------- */

  //! Set L
  void ComputeTemperature::setL(Real L) {this->L = L; }
  
  //! Set C
  void ComputeTemperature::setC(Real C) {this->C = C; }
  
  //! Set rho
  void ComputeTemperature::setRho(Real rho) {this->rho = rho; }
  
  //! Set kappa
  void ComputeTemperature::setKappa(Real kappa) {this->kappa = kappa; }


void ComputeTemperature::compute_kappa(Matrix<std::complex<double>> &fft_coord, Matrix<complex> &To_fft, Matrix<complex> &hv_fft, UInt N) {
	int i_target_r = 0;
	int i_target_i = 0;
	double current_large = abs(hv_fft(0,0).real());
	int break_icon = 0;
	
	for (int r = 0;r < N; r++) {
		for (int i = 0; i < N; i++) {
			if (abs(hv_fft(r,i))>0.001 ) {
				i_target_r = r;
				i_target_i = i;
				break_icon = 1;
				break;
			}
		}
		if (break_icon == 1){ break;}
	}
	
	double fft_coord_d = fft_coord(i_target_r,i_target_i).real();
	kappa = abs(hv_fft(i_target_r,i_target_i))/(abs(To_fft(i_target_r,i_target_i)) * (fft_coord_d));
	//printf("Kappa in compute_kappa: %g\n",kappa);
}


void ComputeTemperature::compute(System& system) {


	UInt size = system.getNbParticles(); // total number of material points

	UInt N = std::sqrt(size); // dimension of square matrix

	Matrix<complex> matT(N);
	
	UInt r;
	UInt i;
	Real Temp;
	Real heat_rate;
	
	double tmp_var; // JUST FOR TESTING
	
	/*-------------------------------------------------------------------------------------------------*/
	// BLOCK 1
	
	Matrix<complex> To(N);
	Matrix<complex> hv(N);
	Matrix<complex> Tn(N);
	Matrix<std::complex<double>> fft_coord = FFT::computeFrequencies(N,system.getL());
	
	double x, y;
	// Use this k to control the type of the heat source and the equillibrium:
	int k = 1;
	
	/*-------------------------------------------------------------------------------------------------*/
	
	// get temperature and heat rates and write it to matrix:	
	for (r=0;r<N;r++) {
		
		//printf("Row %d ********************\n",r);
		for (i=0;i<N;i++) {
		
			Particle& par = system.getParticle(i + r*N);
			  
			auto& mpp = static_cast<MaterialPoint&>(par);

			Temp = mpp.getTemperature();
			heat_rate = mpp.getHeatRate(); 
			
			To(r,i) = Temp;
			hv(r,i) = heat_rate;
			
			//printf("%g	",fft_coord(r,i).real());		
		}
	}
	
	/*-------------------------------------------------------------------------------------------------*/
	 

	// FFT
	Matrix<complex> To_fft = FFT::transform(To);
	Matrix<complex> hv_fft = FFT::transform(hv);
	Matrix<complex> dT_fft(N);

	// set parameters:
	this->setL(system.getL());
	this->setRho(system.getRho());
	this->setC(system.getC());	
	this->setKappa(system.getKappa());

	// Compute kappa (used for test)
	if (kappa<0){ // default value is kappa = -1
		compute_kappa(fft_coord, To_fft, hv_fft, N); // used for Exercise 4.3
	}
	
	/*
	printf("L: %f\n", L);
	printf("Rho: %f\n", rho);
	printf("C: %f\n", C);
	printf("Kappa: %f\n", kappa);
	*/
	
	
	/*-------------------------------------------------------------------------------------------------*/

	// Use FFT to compute the heat propagation:
	for (r=0;r<N;r++) {
//	printf("\n");
		for (i=0;i<N;i++) {
			
			double fft_coord_d = fft_coord(r,i).real();
//			printf("%f, ", fft_coord_d);
			dT_fft(r,i) = 1/(rho * C) * (hv_fft(r,i) - kappa * To_fft(r,i) * (fft_coord_d)); 
		}
	}
		
	// carry out backward transformation:	
	Matrix<complex> dT = FFT::itransform(dT_fft);

	// compute new temperature:
	for (r = 0; r < N; r++) {
	    for (i = 0; i < N; i++) {		    
	    	
	    	Particle& par = system.getParticle(i + r*N);
			  
		auto& mpp = static_cast<MaterialPoint&>(par);
		
		
	    	
		
		if (system.getToggle() == 1) { // boundary condtion according Exercise 4.5
		
		  if (mpp.getTempBCMarker() == 0) {
		  
		    Tn(r,i) = To(r,i)+dT(r,i)*dt;
		  
		  }
		  else if (mpp.getTempBCMarker() == 1) {
		  
		    Tn(r,i) = 0;
		  
		  }
		
		}
		else if (system.getToggle() == 0) { // no boundary condition
		
		  Tn(r,i) = To(r,i)+dT(r,i)*dt;
		  

		}
		
		//printf("bc_marker: %g\n",mpp.getTempBCMarker());
		//printf("Temperature: %g\n",Tn(r,i).real());
		
	    	mpp.getTemperature() = Tn(r,i).real(); // assign computed temperature to material point object
	    }
	} 
	  
	/*-------------------------------------------------------------------------------------------------*/

		  
		
	


}

/* -------------------------------------------------------------------------- */
