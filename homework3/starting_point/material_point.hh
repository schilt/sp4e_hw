#ifndef __MATERIAL_POINT__HH__
#define __MATERIAL_POINT__HH__

/* -------------------------------------------------------------------------- */
#include "particle.hh"

//! Class for MaterialPoint
class MaterialPoint : public Particle {
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  void printself(std::ostream& stream) const override;
  void initself(std::istream& sstr) override;

  Real & getTemperature(){return temperature;};
  Real & getHeatRate(){return heat_rate;};
  Real & getTempBCMarker(){ return temp_bc_marker; };
  
private:
  Real temperature;
  Real heat_rate;
  Real temp_bc_marker; // marker for temperature boundary conditions (bc) that can be passed on to computation; if=0: no bc; used for Ex4.5
};

/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINT__HH__
