Student names:
Baiwei Guo
Ueli Schilt

/*---------------------------------------------------------------------------------------------------------------------------------------*/

Exercise 1: Describe in your README how the particles are organized in the various objects.

Answer:

The "System" class contains all the created particles. Each particle in turn is defined with its properties by the "Particle" class. While "Particle" is the mother class, the specific particles are defined with the respective daughter-class, depending on what type of particle it is: "PingPongBall", "Planet", or "MaterialPoint". While the "Particle" class contains members that apply to all types of particles (e.g. position), the daughter-classes contain members that are only relevant for the specific type of particle (e.g. temperature and heat_rate for MaterialPoint).

The class "ParticlesFactoryInterface" is the motherclass for the classes managing the creation and simulation of particles.

A simulation is called with the createSimulation(...) function of the respective particles_factory_interface class.

The function evolve(...) from the SystemEvolution class calls the compute function of the respective class (e.g. from ComputeTemperature), carrying out the computation for the system (each particle) for each timestep. Here, the results are also sent to the CsvWriter for dumping in the respective intervals.

"Compute" is the motherclass for all computations that are being carried out in the simulation (i.e. contact and gravity forces, temperature evolution, energy calculations, and integration scheme implementation).

/*---------------------------------------------------------------------------------------------------------------------------------------*/

Exercises 4.2 - 4.4: Tests

The tests are implemented looking at one timestep only. The initial point is the equilibrium point is the equilibrium point. It is checked if the temperatures are still the same after one timestep.

Run the tests (Exercises 4.2, 4.3, and 4.4)
- from the terminal, launch the executable 'test_compute_temperature': $ ./test_compute_temperature

/*---------------------------------------------------------------------------------------------------------------------------------------*/

Exercise 4.5: 

Heat distribution is generated in the following Python script: generate_input_materialPoint.py

The file can be launched in the terminal: $ python3 nOfParticles filename
Example: $ python3 256 a_input_Ex4_5.csv

The type of data generated can be selected using 'toggleExercise':
- if set to 0: a homogenous temperature field and heat distribution is generated
- if set to 1: a temperature field and heat source according Exercise 4.5 is generated

"Explain in the README how you integrate this condition within the existing code":

Answer:
- We add a boundary condition argument (bc) to the main function, which decides if the boundary condition is applied or not (1 or 0).
- A member 'UInt toggle_exercise', as well as the functions 'setToggle()' and 'getToggle()' are added to the SystemEvolution class.
- A member 'UInt toggle_exercise', as well as the functions 'setToggle()' and 'getToggle()' are added to the System class.
- With evol.setToggle(bc) in main.cc the input is passed on to the SystemEvolution object (evol), assigning the respective value to the member 'UInt toggle_exercise' of SystemEvolution.
- In the 'evolve()' function of SystemEvolution, we pass the toggle-value to the System class, with 'system->setToggle(this->getToggle())'.
- In the 'compute()' function of the ComputeTemperature class, we retrieve the toggle-value from the system, with 'system.getToggle()'.
- Using an if-statement, all the values of the temperature field are set to 0 where required according to the boundary condition.
- When generating the csv input data with generate_input_materialPoint.py, we add column 'temp_bc_marker', which adds a boundary condition (bc) marker to each point.
- We add a member 'Real temp_bc_marker' to the MaterialsPoint class, as well as a function 'getTempBCMarker()'. This way the marker can be retrieved from the input from the sstr, assigned to the member, and later be retrieved with 'getTempBCMarker()'.
- This marker is then retrieved in the compute function with 'mpp.getTempBCMarker()', where mpp is the MaterialsPoint object. If the marker is 1, the temperature is set to 0, if the marker is 0, the temperature is computed with the method.

/*---------------------------------------------------------------------------------------------------------------------------------------*/
Exercise 4.6: How to launch the simulation

1) Generate input:

Input data (which is the initial temperature) is generated using the following Python script: generate_input_materialPoint.py

The file can be launched in the terminal: $ python3 nOfParticles filename
Example: $ python3 262144 a_input_512_Ex4_5.csv

An example file named a_input_512_Ex4_5.csv is on git.

The type of data generated can be selected using 'toggleExercise'inside the script:
- if set to 0: a homogenous temperature field and heat distribution is generated
- if set to 1: a temperature field and heat source according Exercise 4.5 is generated, while boundary condition markers are included.

The script produces a csv file, which must later be copied into the same folder from where the simulation is launched (e.g. from /build)

2) Build executables:
- Build the code using ccmake from the starting_point folder
  - $ mkdir build
  - $ cd build
  - $ ccmake ..
  - ..
  - $ make
  
3) Run the code from main:
- create a folder called 'dumps' in the same folder where the executables are located
- from the terminal, launch the executable 'particles' with the required arguments.
- The inputs are as follows: $ ./particles nsteps dump_freq input.csv particle_type timestep L* rho* C* kappa* bc*
- arguments with (*) are only required if particle type is material_point
- bc must be set to 1 for Exercise 4.5 (boundary condition enabled)
- Example: $ ./particles 10 1 a_input_512_Ex4_5.csv material_point 0.1 4 1 1 1 1
- the dumps will be saved in the folder 'dumps', where they can also be retrieved for use in Paraview.

Show the simulation in Paraview:

1. Open Paraview
4. Click to the open menu, and load the pack of files altogether (from the dumps folder). Paraview should detect the csv format.
5. In the Properties tab change the delimiter to be a simple space character. You should see a table of numbers corresponding to the material points.
6. To vizualize in 3D, we have to declare to Paraview which are the coordinates. To do this we create a set of points by using the filter 'Table To Points' from the filter menu. You have to select which columns are the X, Y and Z coordinates in the Properties tab.
7. Assigning colors to the temperature value: in the properties tab, open the dropdown menu under 'Coloring'. Select 'T'. Below, click on 'Rescale to Data Range'.
8. The eye button in the pipeline browser lets you display data in the render area. You can now hit the start button (on top) to play the recorded simulation. The points will change colors according to their temperature.

/*---------------------------------------------------------------------------------------------------------------------------------------*/





