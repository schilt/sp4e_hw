Homework 4

Student names: Ueli Schilt, Baiwei Guo

Exercise 1, Task 2: In class ParticlesFactory, createSimulation function has been overloaded to take functor as one
of its argument. Comment on what this function is doing?

Answer: The function is called in the main.py file: evol = factory.createSimulation(filename, timestep, createComputes). This function is used in case the particle is of type "planet" or "material_point". In this case, an aditional argument is passed to the function createSimulation, the functor "createComputes", which is defined in the main.py file. This functor helps passing additional values to the computation (e.g. capacity, length, G, ...).

Exercise 2, Task 2: How will you ensure that references to Compute objects type are correctly managed in the python
bindings?

Answer: A template type that denotes a special holder type is passed to the binding generator (class_) in order to manage references to the object. In this case, std::shared_ptr is used.

 Example: py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

Exercise 4, Task 2: Terminal command in 'starting_point' directory: $ python3 main.py 365 1 init.csv planet 1.0
  -> results are stored in ./starting_point/Ex4_2_dumps
  
Exercises 5 and 6: All the functions can be found in the run_and_compute_error.py file inside the 'starting_point' directory.

Exercise 7: The routine is implemented in the 'scale_optimizer.py' file inside the 'starting_point' directory.
  
-----------------------------------------------------------------------------------------------------
HOW TO RUN THE CODE:

First, the code must be built and compiled (e.g. with cmake) from the 'starting_point' directory.

When building the code, the variable USE_PYTHON must be set to 'ON'.

Then, follow these steps:

Part I: Launch particle code via main.py

  Launch particle code from terminal in 'starting_point' directory: $ python3 main.py nsteps dump_freq input.csv particle_type timestep

    Example: $ python3 main.py 365 1 init.csv planet 1.0
  
  Note:
  - The input file (i.e. init.csv) must be stored in the 'starting_point' folder as well.
  - The code will automatically create a 'dumps' folder inside 'starting_point' and store the resulting files in this folder.
  
Part II: Run optimisation

  1. Navigate to the 'starting_point' directory
  2. Ensure that the input-file is in the 'starting_point' directory (e.g. init.csv)
  3. Open the 'scale_optimizer.py' file: under 'user input', adjust the input-parameters (if required) and save the file.
     The following parameters can be adjusted:
        - scale_0 = 1.0 # starting point for the optimisation
        - planet_name = 'mercury'
        - input_0 = 'init.csv' # initial input file (no scaling)
        - nb_steps = 365
        - freq = 1
  4. Run the module from the terminal: $ python3 scale_optimizer.py
  5. The following output will be created:
        - A 'dumps' folder where the interim csv files are stored
        - A 'results_files' folder, where the plot of Ex7 and a csv with corresponding values is stored
        - An 'init_scaled.csv' file in the 'starting_point' directory
        - The correctly scaled initial velocity of Mercury can now be found in init_scale.csv

  
