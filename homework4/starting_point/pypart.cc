#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "system.hh"
#include "system_evolution.hh"

#include "compute.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"

#include "compute_temperature.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  /* -------------------------------------------------------------------------------------------------- */
  // ParticlesFactory Classes:
  
  // binding mother-class ParticlesFactoryInterface:
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
      .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
      .def("getSystemEvolution", &ParticlesFactoryInterface::getSystemEvolution);

  
  // binding daughter-class PlanetsFactory:
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
      .def("createSimulation", static_cast<SystemEvolution& (PlanetsFactory::*)(const std::string&, Real)>(&PlanetsFactory::createSimulation), py::return_value_policy::reference)
      .def("createSimulation", static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real, py::function)>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
      .def_property_readonly("system_evolution", &PlanetsFactory::getSystemEvolution, py::return_value_policy::reference)
      .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);
      
  // binding daugher-class MaterialPointsFactory:
  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
      .def("createDefaultComputes", &MaterialPointsFactory::createDefaultComputes)
      .def("createSimulation", static_cast<SystemEvolution& (MaterialPointsFactory::*)(const std::string&, Real)>(&MaterialPointsFactory::createSimulation), py::return_value_policy::reference)
      .def("createSimulation", static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real, py::function)>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
      .def_property_readonly("system_evolution", &MaterialPointsFactory::getSystemEvolution, py::return_value_policy::reference)
      .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);
  
  // binding daughter-class PingPongBallsFactory:
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
      .def("createSimulation", static_cast<SystemEvolution& (PingPongBallsFactory::*)(const std::string&, Real)>(&PingPongBallsFactory::createSimulation), py::return_value_policy::reference)
      .def("createSimulation", static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real, py::function)>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
      .def_property_readonly("system_evolution", &PingPongBallsFactory::getSystemEvolution, py::return_value_policy::reference)
      .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);
  
  
  /* -------------------------------------------------------------------------------------------------- */
  // Compute Classes:
  
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");
  
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");
  
  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
      .def(py::init<>())
      .def("compute", &ComputeGravity::compute)
      .def("setG", &ComputeGravity::setG);
      
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
      .def("compute", &ComputeTemperature::compute)
      .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::setConductivity)
      .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::setCapacity)
      .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::setDensity)
      .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
      .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::setDeltat);
      
  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
      .def(py::init<Real>())
      .def("setDeltaT", &ComputeVerletIntegration::setDeltaT)
      .def("compute", &ComputeVerletIntegration::compute)
      .def("addInteraction", &ComputeVerletIntegration::addInteraction);
  
  /* -------------------------------------------------------------------------------------------------- */
  // SystemEvolution class:
  
  py::class_<SystemEvolution>(m, "SystemEvolution")
      .def("evolve", &SystemEvolution::evolve)
      .def("addCompute", &SystemEvolution::addCompute)
      .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
      .def("setNSteps", &SystemEvolution::setNSteps)
      .def("setDumpFreq", &SystemEvolution::setDumpFreq);
      
  /* -------------------------------------------------------------------------------------------------- */
  // System class:
  
  py::class_<System>(m, "System")
      .def(py::init<>());
      
  /* -------------------------------------------------------------------------------------------------- */
  // CsvWriter class:
  
  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
      .def(py::init<const std::string&>())
      .def("write", &CsvWriter::write)
      .def("compute", &CsvWriter::compute);

  
}
