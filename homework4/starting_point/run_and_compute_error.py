import pandas as pd
import numpy as np
from numpy import linalg as LA
import os
from main import main


def generateInput(scale,planet_name,input_filename,output_filename):

    """
    Generate an input file from a given input file but by scaling velocity of
    a given planet.
    """

    # Read input file with pandas:
    df = pd.read_csv(input_filename, delimiter=r"\s+")
    
    # Find the row-index of the selected planet (e.g. mercury):
    index_planet = np.where(df['name']== planet_name)
    index_planet = index_planet[0][0]
    
    # Create datafrmae with velocities of respective planet:
    df_velocity_planet = df.iloc[index_planet:index_planet+1,3:6]
    
    # Scale velocities:
    df_velocity_planet.iat[0,0] = df_velocity_planet.iat[0,0]*scale #VX
    df_velocity_planet.iat[0,1] = df_velocity_planet.iat[0,1]*scale #VY
    df_velocity_planet.iat[0,2] = df_velocity_planet.iat[0,2]*scale #VZ

    # Replace values in dataset with scaled velocities:
    df.iloc[index_planet:index_planet+1,3:6] = df_velocity_planet
    
    # Print scaled input to a csv file:
    df.to_csv(output_filename, sep=' ', index=False)

def readPositions(planet_name,directory):

    """
    Read the trajectory of a given planet and make a numpy array out of it.
    """

    n_max = 0
    for path in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, path)):
            n_max += 1

    if len(os.listdir(directory)[2]) == 13:
        num_size = '04d'
    else:
        num_size = '05d'
    n_str = format(0, num_size)

    str_1 = '/step-'
    str_2 = '.csv'
    
    # Create first filename:
    dir_list = list(directory)+list(str_1)+list(n_str)+list(str_2)
    filename = ''.join(dir_list)
    
    # Initialise array with first position:
    position = read_position(planet_name,filename)
    positions = np.array([position])
    
    for i in range(1,n_max):
    
        # Create filename:
        n_str = format(i, num_size)
        dir_list = list(directory)+list(str_1)+list(n_str)+list(str_2)
        filename = ''.join(dir_list)
        
        # Read position and add to array:
        positions = np.append(positions,[read_position(planet_name,filename)],axis=0)

    return positions
    
    
def read_position(planet_name,filename):

    """
    Helper-function for readPositions().
    Read the position of a given planet at one timestep and return the position.
    """
    
    # Read csv-file:
    df = pd.read_csv(filename, delimiter=r"\s+",header=None)   
    if df.iloc[0,0]=='#X':
        df = pd.read_csv(filename, delimiter=r"\s+")
        
    # Convert to numpy array:
    df_mat = df.to_numpy()
    
    # Find the row-index of the selected planet (e.g. mercury):
    index_planet = np.where(df_mat[:,-1] == planet_name)
    index_planet = index_planet[0][0]
    
    # Only keep selected row:
    df_row = df_mat[index_planet,:]
    
    # Extract position (columns 1-3):
    pos_planet = df_row[0:3]
    
    return pos_planet

def computeError(positions,positions_ref):

    """
    Compute the error out of two numpy trajectories.
    """

    # the position should be a 2d array, each row is a position at some time step
    error_square = 0
    n_steps = len(positions)
    for i in range(0,n_steps):
        error_square = error_square+ pow(LA.norm(positions[i]-positions_ref[i]),2)
        
    return pow(error_square,1/2)


def launchParticles(input_, nb_steps, freq):

    """
    Launch particle code on a provided input.
    """
    
    # assign parameters for type planet:
    filename = input_
    particle_type = 'planet'
    timestep = 1.0 # 1 day
    
    # launch particles code:
    main(nb_steps, freq, filename, particle_type, timestep)

    return 0


def runAndComputeError(scale, planet_name, input_, nb_steps, freq):

    """
    Give the error for a given scaling velocity factor of a given planet.
    """

    input_filename = input_
    output_filename = 'init_scaled.csv'

    generateInput(scale,planet_name,input_filename,output_filename)
    
    scaled_input = output_filename
    
    launchParticles(scaled_input, nb_steps, freq)
    
    positions_ref = readPositions(planet_name, 'trajectories')
    positions_scaled = readPositions(planet_name, 'dumps')
    
    error = computeError(positions_scaled, positions_ref)
    
    return error






