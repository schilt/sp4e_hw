import os
import scipy
from scipy import optimize
import matplotlib.pylab as plt
import pandas as pd
from run_and_compute_error import runAndComputeError

#-----------------------------------------------------------------------------#
# User input:

scale_0 = 1.0 # starting point for the optimisation
planet_name = 'mercury'
input_0 = 'init.csv' # initial input file (no scaling)
nb_steps = 365
freq = 1

#-----------------------------------------------------------------------------#
# create results_files folder (if non-existent) for storing results:

if os.path.isdir('results_files') == True:
    pass
else:
    os.mkdir('results_files')
    
results_file_name = 'results_files/results_scale_error.csv'
results_plot_name = 'results_files/optimisation_plot.png'

#-----------------------------------------------------------------------------#
# Optimisation:

Nf_eval = 1 # Initialise evaluation counter

# Create lists to store scaling factor and error of each optimisation step:
list_scaling_factors = []
list_errors = []

# Redefine runAndComputeError function to optimise for scale:
def runAndComputeErrorOpt(scale_opt):
    return runAndComputeError(scale_opt, planet_name, input_0, nb_steps, freq)

# Callback function to retrieve scale-factor at each iteration:
def callbackF(scale_i):
    global Nf_eval
    print("{0:4d} {1:3.6f} {2:3.6f}".format(Nf_eval, scale_i[0], runAndComputeErrorOpt(scale_i[0])))
    list_scaling_factors.append(scale_i[0])
    list_errors.append(runAndComputeErrorOpt(scale_i[0]))
    Nf_eval+=1

# Print header:
print("{0:4s} {1:9s} {2:9s}".format("Iter", "Scale", "Error"))

# Carry out optimisation:
minimum = optimize.fmin(runAndComputeErrorOpt, scale_0, callback=callbackF)

print("Optimal scale: {}".format(minimum[0]))

#-----------------------------------------------------------------------------#
# Generate plot:

fig, ax1 = plt.subplots(dpi=300)

ax1.plot(list_scaling_factors, color='b', label = 'Scaling factor')
ax1.set_xlabel('Iteration')
ax1.set_ylabel('Scaling factor')

ax2=ax1.twinx() # create secondary axis
ax2.plot(list_errors, color='r', label = 'Error')
ax2.set_ylabel('Error')

# ask matplotlib for the plotted objects and their labels
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc=0)


fig.savefig(results_plot_name, bbox_inches='tight')
#plt.show()

#-----------------------------------------------------------------------------#
# Save results to csv file:

dict_results = {'scale':list_scaling_factors,
                'error':list_errors
                }
                
df_results = pd.DataFrame(dict_results, columns = ['scale', 'error'])
df_results.to_csv(results_file_name)

#-----------------------------------------------------------------------------#
# Print information:
print("\nComputed errors and scaling factors have been saved in ", results_file_name, "\n")
print("Plot has been saved as ", results_plot_name, "\n")
print("The correctly scaled initial velocity of the planets can be found in init_scale.csv")

#-----------------------------------------------------------------------------#



