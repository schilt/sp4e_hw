"""
SP4E Homework Assignment - Week 4
Students:
- Baiwei Guo
- Ueli Schilt

"""

"""EXERCISE 1, TASK 1"""

# import required packages:
import numpy as np
import scipy
from scipy import optimize
from scipy.sparse.linalg import lgmres
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

Nfeval = 1 # initialize counter to print interim results
x_rec = [] # initialize list to store interim x vector values
cost_rec = [] # intitialize list to store interim function values

# objective function which we want to minimize:
def fun_S(x):
    A = np.array([[8, 1],[1, 3]])
    b = np.array([[2],[4]])
  
    return 1/2*x.T@A@x-x.T@b

# callback function to obtain interim results:
def callbackF(Xi):
    global Nfeval
    global x_rec
    global cost_rec
    print ('{0:4d}   {1: 3.3f}   {2: 3.3f}  '.format(Nfeval, Xi[0], Xi[1]))
    x_rec.append(Xi)
    cost_rec.append(fun_S(Xi))
    Nfeval += 1



"""BFGS Method:"""
ret_bfgs = scipy.optimize.minimize(fun_S, np.array([[2],[2]]), tol=1e-5, method="BFGS",callback=callbackF)
x_rec_bfgs = x_rec # iteration points (x,y)
cost_rec_bfgs = cost_rec # iteration values (z)

# Preparing the iteration points for plotting later (use of pandas):
df1 = pd.DataFrame(x_rec_bfgs)
df1[2] = np.ravel(cost_rec_bfgs)

x1 = df1[0]
y1 = df1[1]
z1 = df1[2]

"""LGMRES Method:"""
# new initialization:
Nfeval = 1
x_rec = []
cost_rec = []

A = np.array([[8, 1],[1, 3]])
b = np.array([[2],[4]])


ret_lgmres =lgmres(A, b, x0=np.array([[2],[2]]), tol=1e-05, maxiter=1000, callback=callbackF)

# Preparing the iteration points for plotting later:
ret_rec_lgmres = np.concatenate((np.array([[2,2]]), np.array(x_rec[1:])), axis=0)

df2 = pd.DataFrame(ret_rec_lgmres)


x2=np.array(df2[0])
y2=np.array(df2[1])
z2=np.array([cost_rec[0][0],cost_rec[1][0]])



"""EXERCISE 1, TASK 2"""

# helper function for plotting:
def fun(x, y):
    
    Z = []
    
    for i in np.arange(0,len(x)):
    
        vec = np.array([x[i],y[i]])

        Z.append(1/2*vec.T@A@vec-vec.T@b)
    
    Z=np.array(Z)
    
    return Z


'''Plotting BFGS method:'''
fig1 = plt.figure()
ax1 = plt.axes(projection='3d')
ax1.set(title = "BFGS method", xlabel = "x", ylabel="y",zlabel="z")

# Make data:
X = np.arange(-3, 3, 0.1)
Y = np.arange(-3, 3, 0.1)
X, Y = np.meshgrid(X, Y)
Z = np.array(fun(np.ravel(X), np.ravel(Y)))
Z = Z.reshape(X.shape)

# Plot the surface.
surf = ax1.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False, alpha=0.3)

# plot the iteration points:
ax1.scatter(x1, y1, z1, c="red",s=6,alpha=1.0, marker='o')
# connect the iteration points
ax1.plot(x1,y1,z1,color="r", linewidth=1,ls='--')

# Customize the z axis:
ax1.set_zlim(0, 70)
ax1.zaxis.set_major_locator(LinearLocator(10))
ax1.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# rotate the plot for better view:
ax1.view_init(45,-45) 

# Add a color bar which maps values to colors:
fig1.colorbar(surf, shrink=0.5, aspect=5)

# save the plot as png:
fig1.savefig("BFGS_plot.png", dpi=1200)

#plt.show()


'''Plotting LGMRES method:'''
fig2 = plt.figure()
ax2 = plt.axes(projection='3d')
ax2.set(title = "LGMRES method", xlabel = "x", ylabel="y",zlabel="z")

# Make data.
X = np.arange(-3, 3, 0.1)
Y = np.arange(-3, 3, 0.1)
X, Y = np.meshgrid(X, Y)
Z = np.array(fun(np.ravel(X), np.ravel(Y)))
Z = Z.reshape(X.shape)

# Plot the surface.
surf = ax2.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False, alpha=0.3)

# plot the iteration points:
ax2.scatter(x2, y2, z2, c="red",s=6,alpha=1.0, marker='o')
# connect the iteration points
ax2.plot(x2,y2,z2,color="r", linewidth=1,ls='--')

# Customize the z axis:
ax2.set_zlim(0, 70)
ax2.zaxis.set_major_locator(LinearLocator(10))
ax2.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# rotate the plot for better view:
ax2.view_init(45,-45)

# Add a color bar which maps values to colors:
fig2.colorbar(surf, shrink=0.5, aspect=5)

# save the plot as png:
fig2.savefig("LGMRES_plot.png", dpi=1200)

#plt.show()


