"""
SP4E Homework Assignment - Week 4
Students:
- Baiwei Guo
- Ueli Schilt

"""

"""EXERCISE 2"""

import numpy as np
import pandas as pd
import scipy 
from scipy.sparse.linalg import lgmres
import argparse
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

parser = argparse.ArgumentParser()

parser.add_argument('-m','--minimizer',type=str,help='type: str. Select minimizer. Options: \'BFGS\',\'LGMRES\',\'GMRES\'',required=True)
parser.add_argument('-pl','--GMRES_plot', type=bool,help='type: bool. Set to \'True\' for plotting the iteration results.',required=True)
parser.add_argument('-A','--matrix_A', type=float, nargs='+', action='append', help='n x n matrix of system Ax=b; Enter each matrix row seperately: -A a11 a12 ... -A a21 a22 ... ',required=True)
parser.add_argument('-b','--vector_b', type=float, nargs='+', action='append', help='vector of length n of system Ax=b; Enter each vector row seperately: -b b11 -b b b21 ... ',required=True)

args = parser.parse_args()


A = np.array(args.matrix_A)
b = np.array(args.vector_b)
GMRES_plot=args.GMRES_plot # plot yes/no
m = args.minimizer # type of minimizer

#Values from exercise:
#A = np.array([[8,1],[1,3]])
#b = np.array([[2],[4]])




Nfeval = 1
x_rec = []
x_gmres = [] # list to capture iteration results
cost_rec = []
def fun_S(x):
    ## Insert your objective function here
    ## Input: an np array vector
    ## Output: a number
    
    return 1/2*x.T@A@x-x.T@b

def callbackF(Xi):
    ## Operation on the iterations of BFGS and 
    ## Input: an np array vector
    ## Output: lists of np array vectors (global x_rec, cost_rec)
    global Nfeval
    global x_rec
    global cost_rec
    print ('{0:4d}   {1: 3.3f}   {2: 3.3f}  '.format(Nfeval, Xi[0], Xi[1]))
    x_rec.append(Xi)
    cost_rec.append(fun_S(Xi))
    Nfeval += 1

def gmres(x_ini,A,b,thres):
    ## GMRES to solve Ax=b
    ## Need the matrix A to be at least 2 rows
    d = A.shape[0]
    r0 = b-np.einsum('ik,k->i',A,x_ini)
    n = len(A)
    V=[]
    norm_r0 = norm_ein(r0)
    V.append(r0/norm_r0)
    b_norm = norm_ein(b)
    error = (norm_r0/b_norm)
    V_mat = np.array(V).transpose()
    H=np.zeros((n+1,n))
    sn = np.zeros(d)
    cs = np.zeros(d)
    e1 = np.zeros(d+1)
    e1[0] = 1
    e = [error]
    beta = norm_r0 * e1
    
    global x_gmres
    
    ## Normalization 
    for m in range(1, n+1):
        V_mat = V_mat[:,0:m]
        v_m_new_orig = mat_dot_vec(A,V[-1])
        h_new_vec = mat_dot_vec(V_mat.T,v_m_new_orig)
        v_m_new =  v_m_new_orig - mat_dot_vec(V_mat,h_new_vec)
        h_new_element = norm_ein(v_m_new)
        H[0:m,m-1] = h_new_vec
        H[m,m-1] = h_new_element
        if abs(h_new_element) < 10e-6:
            break
        V.append(v_m_new/h_new_element)
        V_mat = np.array(V).transpose()
    
    m_V = V_mat.shape[1]
    H_original = H
    print(x_ini)
    for m in range(1, m_V+1):
        H_temp = H_original[0:m+1,0:m]
        H[0:m+1,m-1], cs[m-1],sn[m-1] = apply_rotations(H_temp[0:m+1,m-1], cs, sn, m)
        beta[m] = -sn[m-1] * beta[m-1]
        beta[m-1] = cs[m-1] * beta[m-1]
        error = abs(beta[m]) / b_norm
        e.append(error)
        y = mat_dot_vec(np.linalg.inv(H[0:m, 0:m]),beta[0:m])
        x_gmres.append(x_ini + mat_dot_vec(V_mat[:,0:m],y))
        print(x_ini + mat_dot_vec(V_mat[:,0:m],y))
        if error <= thres:
            break
    y = mat_dot_vec(np.linalg.inv(H[0:m, 0:m]),beta[0:m])
    return x_ini + mat_dot_vec(V_mat[:,0:m],y)

    
def apply_rotations(h, cs, sn, k):
    ## QR decomposition on H
    for i in range(0,k-1):
        temp   =  cs[i] * h[i] + sn[i] * h[i + 1]
        h[i + 1] = -sn[i] * h[i] + cs[i] * h[i + 1]
        h[i]   = temp
    cs_k, sn_k = givens_rotation(h[k-1], h[k])
    h[k-1] = cs_k * h[k-1] + sn_k * h[k]
    h[k] = 0.0
    return h, cs_k, sn_k

def givens_rotation(v1, v2):
    t = pow(v1**2 + v2**2,1/2)
    cs = v1 / t 
    sn = v2 / t
    return cs, sn

def norm_ein(a):
    # a is a np vector array
    return pow(np.einsum('k,k->',a,a),1/2)

def mat_dot_vec(A,b):
    # b is a np vector array
    return np.einsum('ik,k->i',A,b)

def mat_dot_mat(A,B):
    # b is a np vector array
    return np.einsum('ij,jk->ik', A, B)


######### Select optimizer:

#A = np.array([[8, 1],[1, 3]])
#b = np.array([2,4])

b = np.ravel(b)


if m == 'BFGS':
    ret_bfgs = scipy.optimize.minimize(fun_S, np.array([[2],[2]]), tol=1e-5, method="BFGS",callback=callbackF)
    x_rec_bfgs = x_rec
    cost_rec_bfgs = cost_rec
elif m == 'LGMRES':
    Nfeval = 1
    x_rec = []
    cost_rec = []
    #A = np.array([[8, 1],[1, 3]])
    #b = np.array([2,4])
    ret_lgmres =lgmres(A, b, x0=np.array([2,2]), tol=1e-05, callback=callbackF)
elif m == 'GMRES':

    x_ini = np.array([2,2])
    x_sol = gmres(x_ini,A,b,10**(-5))
else:
    print("Error: minimizer not properly defined. Options are: \'BFGS\',\'LGMRES\',\'GMRES\'")


if (m == 'GMRES') and (GMRES_plot == True):
    
    # helper function for plotting:
    def fun(x, y):
        
        Z = []
        
        for i in np.arange(0,len(x)):
        
            vec = np.array([x[i],y[i]])
    
            Z.append(1/2*vec.T@A@vec-vec.T@b)
        
        Z=np.array(Z)
        
        return Z
    
    # Preparing data for plotting:
    
    df = pd.DataFrame([x_ini])
    df = df.append(pd.DataFrame(x_gmres))
    df = df.reset_index()
  
    x1=np.array(df[0])
    y1=np.array(df[1])
    z1=np.array(fun(df[0],df[1]))
    
    '''Plotting GMRES method:################################################################'''
    fig1 = plt.figure()
    ax1 = plt.axes(projection='3d')
    ax1.set(title = "GMRES method", xlabel = "x", ylabel="y",zlabel="z")
    
    # Make data:
    X = np.arange(-3, 3, 0.1)
    Y = np.arange(-3, 3, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = np.array(fun(np.ravel(X), np.ravel(Y)))
    Z = Z.reshape(X.shape)
    
    # Plot the surface.
    surf = ax1.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False, alpha=0.3)
    
    # plot the iteration points:
    ax1.scatter(x1, y1, z1, c="red",s=6,alpha=1.0, marker='o')
    # connect the iteration points
    ax1.plot(x1,y1,z1,color="r", linewidth=1,ls='--')
    
    # Customize the z axis:
    ax1.set_zlim(0, 70)
    ax1.zaxis.set_major_locator(LinearLocator(10))
    ax1.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    
    # rotate the plot for better view:
    ax1.view_init(45,-45) 
    
    # Add a color bar which maps values to colors:
    fig1.colorbar(surf, shrink=0.5, aspect=5)
    
    # save the plot as png:
    fig1.savefig("GMRES_plot.png", dpi=1200)
    
    #plt.show()
