Homework Assignment Week 4
Students:
Baiwei Guo
Ueli Schilt

How to launch the scripts:

Exercise 1: optimizer.py
- Run script from terminal (e.g. $python ./optimizer.py)
- The plots will be saved as .png files in the same folder where the script is stored

Exercise 2: GMRES.py
- Run script from terminal with the 4 required arguments (run "$ python ./GMRES.py -h" to see the options and format of the arguments).
- The plot will be saved as .png file in the same folder where the script is stored
- Example using the following arguments: minimizer 'GMRES', plotting on, matrix A=[[8,1],[1,3]], Vector b=[[2],[4]]:

$ python ./GMRES.py -m 'GMRES' -pl True -A 8 1 -A 1 3 -b 2 -b 4